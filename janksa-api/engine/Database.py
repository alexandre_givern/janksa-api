import pymysql.cursors
import os

class Database:

    def get(sQuery, aData=()):
        oConnection = pymysql.connect(host=os.environ['DB_HOSTNAME'],
                                      user=os.environ['DB_USERNAME'],
                                      passwd=os.environ['DB_PASSWORD'],
                                      db=os.environ['DB_NAME'],
                                      cursorclass=pymysql.cursors.DictCursor)

        try:
            with oConnection.cursor() as oCursor:
                oCursor.execute(sQuery, aData)
                aRows = oCursor.fetchall()
        finally:
            oConnection.close()

        return aRows;

    def insert(sQuery, aData):
        oConnection = pymysql.connect(host=os.environ['DB_HOSTNAME'],
                                      user=os.environ['DB_USERNAME'],
                                      passwd=os.environ['DB_PASSWORD'],
                                      db=os.environ['DB_NAME'],
                                      cursorclass=pymysql.cursors.DictCursor,
                                      charset='utf8')

        try:
            with oConnection.cursor() as oCursor:
                oCursor.execute('SET NAMES utf8;')
                oCursor.execute('SET CHARACTER SET utf8;')
                oCursor.execute('SET character_set_connection=utf8;')
                oCursor.execute(sQuery, aData)
                iLastrowid = oCursor.lastrowid
                oConnection.commit()
        finally:
            oConnection.close()

        return iLastrowid;

    def update(sQuery, aData):
        oConnection = pymysql.connect(host=os.environ['DB_HOSTNAME'],
                                      user=os.environ['DB_USERNAME'],
                                      passwd=os.environ['DB_PASSWORD'],
                                      db=os.environ['DB_NAME'],
                                      cursorclass=pymysql.cursors.DictCursor,
                                      charset='utf8')

        try:
            with oConnection.cursor() as oCursor:
                oCursor.execute('SET NAMES utf8;')
                oCursor.execute('SET CHARACTER SET utf8;')
                oCursor.execute('SET character_set_connection=utf8;')
                oCursor.execute(sQuery, aData)
                oConnection.commit()
        finally:
            oConnection.close()
