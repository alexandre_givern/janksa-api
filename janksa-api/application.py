from flask import Flask, url_for, request, json, render_template
from engine.Database import *
import json
import datetime
import time
import hashlib

def datetime_handler(x):
    if isinstance(x, datetime.datetime):
        return x.strftime("%Y-%m-%d %H:%M:%S")
    raise TypeError("Unknown type")

application = Flask(__name__, static_folder="swagger")

if __name__ == '__main__':
    application.run()

@application.route('/<token>')
def root(token):
    return application.send_static_file('index.html')

@application.errorhandler(404)
def page_not_found(error):
    return 'Not found', 404

### Projects ###################################################################

@application.route('/projects/<token>', methods=['GET'])
def getProjects(token):
    """ Get all the projects """

    m = hashlib.sha1()
    key = "H5812*Ub48J!" + time.strftime("%d%m%Y")
    m.update(key.encode())
    if token != m.hexdigest():
        return "Not found", 404

    sQuery=(
        "SELECT prj_id AS id"
        ", prj_name AS name"
        ", prj_picture AS picture"
        ", prj_fk_mbr_id AS member"
        ", prj_created_at AS date"
        " FROM janksa.projects_prj"
    )

    aData = ()

    aRequestSort = request.args.get('sort_by')

    if aRequestSort != None:
        aRequestSort = json.loads(aRequestSort)
        sQuery += " ORDER BY"

        if aRequestSort['name'] == 'date':
            sQuery += " prj_created_at"
        else:
            sQuery += " prj_created_at"

        if aRequestSort['type'] == 'ASC':
            sQuery += " ASC"
        else:
            sQuery += " DESC"

    aRequestOffset = request.args.get('limit')

    if aRequestOffset != None:
        aRequestOffset = json.loads(aRequestOffset)

        sQuery += " LIMIT %s, %s"
        aData = aData + (aRequestOffset['min'],)
        aData = aData + (aRequestOffset['max'],)

    aRows = Database.get(sQuery, aData)

    response = application.response_class(
        response=json.dumps(aRows, default=datetime_handler),
        status=200,
        mimetype='application/json'
    )
    return response

@application.route('/projects/<token>', methods=['POST'])
def postProjects(token):
    """ Create a project """

    m = hashlib.sha1()
    key = "H5812*Ub48J!" + time.strftime("%d%m%Y")
    m.update(key.encode())
    if token != m.hexdigest():
        return "Not found", 404

    sQuery=(
        "INSERT INTO janksa.projects_prj"
        " (prj_name, prj_picture, prj_fk_mbr_id, prj_created_at)"
        " VALUES (%s, %s, %s, NOW())"
    )

    aData = ()

    aRequestData = request.get_json()

    aData = aData + (aRequestData['name'],)
    aData = aData + (aRequestData['picture'],)
    aData = aData + (aRequestData['member'],)

    iProjectid = Database.insert(sQuery, aData)

    if iProjectid != None :

        if 'description' in aRequestData.keys():
            aData = ()

            sQuery=(
                "INSERT INTO janksa.project_description_prd"
                " (prd_fk_prj_id, prd_description)"
                " VALUES (%s, %s)"
            )

            aData = aData + (iProjectid,)
            aData = aData + (aRequestData['description'],)

            Database.insert(sQuery, aData)


        aData = ()

        sQuery=(
            "INSERT INTO janksa.project_members_prm"
            " (prm_fk_prj_id, prm_fk_mbr_id, prm_fk_prt_id)"
            " VALUES (%s, %s, %s)"
        )

        aData = aData + (iProjectid,)
        aData = aData + (aRequestData['member'],)
        aData = aData + (1,)

        Database.insert(sQuery, aData)

        response = application.response_class(
            status=201,
            mimetype='application/json'
        )
        response.headers['Content-Location'] = url_for('getProject', projectid=iProjectid)

    else:
        response = application.response_class(
            status=400,
            mimetype='application/json'
        )

    return response

@application.route('/projects/<token>/<projectid>', methods=['GET'])
def getProject(token, projectid):
    """ Get a project by his id """

    m = hashlib.sha1()
    key = "H5812*Ub48J!" + time.strftime("%d%m%Y")
    m.update(key.encode())
    if token != m.hexdigest():
        return "Not found", 404

    sQuery = (
        "SELECT prj_id AS id"
        ", prj_name AS name"
        ", prj_picture AS picture"
        ", IFNULL(prd_description,'') AS description"
        ", prj_fk_mbr_id AS member"
        " FROM janksa.projects_prj"
        " LEFT JOIN janksa.project_description_prd ON prd_fk_prj_id = prj_id"
        " WHERE prj_id = %s"
    )

    aData = (projectid)

    aRow = Database.get(sQuery, aData)

    sQuery = (
        "SELECT prc_id AS id"
        ", prc_fk_mbr_id AS member"
        ", prc_comment AS comment"
        " FROM janksa.project_comments_prc"
        " WHERE prc_fk_prj_id = %s"
        " ORDER BY prc_created_at DESC"
    )

    aData = (projectid)

    aComments = Database.get(sQuery, aData)

    sQuery = (
        "SELECT prr_id AS id"
        ", prr_description AS description"
        " FROM janksa.project_recruitments_prr"
        " WHERE prr_fk_prj_id = %s"
        " ORDER BY prr_created_at DESC"
    )

    aData = (projectid)

    aRecruitments = Database.get(sQuery, aData)

    sQuery = (
        "SELECT prn_id AS id"
        ", prn_content AS content"
        ", prn_created_at AS date"
        " FROM janksa.project_news_prn"
        " WHERE prn_fk_prj_id = %s"
        " ORDER BY prn_created_at DESC"
    )

    aData = (projectid)

    aNews = Database.get(sQuery, aData)

    sQuery = (
        "SELECT prm_fk_mbr_id AS member"
        ", prm_fk_prt_id AS type"
        " FROM janksa.project_members_prm"
        " WHERE prm_fk_prj_id = %s"
        " ORDER BY prm_fk_mbr_id DESC"
    )

    aData = (projectid)

    aFollowers = Database.get(sQuery, aData)

    if len(aRow) > 0:
        aRow[0]['comments'] = aComments
        aRow[0]['recruitments'] = aRecruitments
        aRow[0]['news'] = aNews
        aRow[0]['followers'] = aFollowers

        response = application.response_class(
            response=json.dumps(aRow[0], default=datetime_handler),
            status=200,
            mimetype='application/json'
        )

    else:
        response = application.response_class(
            response={},
            status=200,
            mimetype='application/json'
        )
    return response

@application.route('/projects/<token>/<projectid>', methods=['PUT'])
def putProject(token, projectid):
    """ Update a project by his id """

    m = hashlib.sha1()
    key = "H5812*Ub48J!" + time.strftime("%d%m%Y")
    m.update(key.encode())
    if token != m.hexdigest():
        return "Not found", 404

    aRequestData = request.get_json()

    if 'name' in aRequestData.keys():
        aData = ()

        sQuery = (
            "UPDATE janksa.projects_prj"
            " SET prj_name = %s"
            ", prj_picture = %s"
            " WHERE prj_id = %s"
        )

        aData = aData + (aRequestData['name'],)
        aData = aData + (aRequestData['picture'],)
        aData = aData + (projectid,)

        Database.update(sQuery, aData)

    if 'description' in aRequestData.keys():
        aData = ()

        sQuery=(
            "UPDATE janksa.project_description_prd"
            " SET prd_description = %s"
            " WHERE prd_fk_prj_id = %s"
        )

        aData = aData + (aRequestData['description'],)
        aData = aData + (projectid,)

        Database.update(sQuery, aData)

    if 'comment' in aRequestData.keys():
        aData = ()

        sQuery=(
            "INSERT INTO janksa.project_comments_prc"
            " (prc_fk_prj_id, prc_fk_mbr_id, prc_comment)"
            " VALUES (%s, %s, %s)"
        )

        aData = aData + (projectid,)
        aData = aData + (aRequestData['member'],)
        aData = aData + (aRequestData['comment'],)

        Database.insert(sQuery, aData)

    if 'followers' in aRequestData.keys():
        aData = ()

        if 'put' in aRequestData['followers'].keys():
            sQuery=(
                "INSERT INTO janksa.project_members_prm"
                " (prm_fk_prj_id, prm_fk_mbr_id, prm_fk_prt_id)"
                " VALUES (%s, %s, %s)"
                " ON DUPLICATE KEY UPDATE prm_fk_prt_id = %s"
            )

            aData = aData + (projectid,)
            aData = aData + (aRequestData['followers']['member'],)
            aData = aData + (aRequestData['followers']['type'],)
            aData = aData + (aRequestData['followers']['type'],)

            Database.insert(sQuery, aData)
        else:
            sQuery=(
                "DELETE FROM janksa.project_members_prm"
                " WHERE prm_fk_prj_id = %s"
                " AND prm_fk_mbr_id = %s"
            )

            aData = aData + (projectid,)
            aData = aData + (aRequestData['followers']['member'],)

            Database.insert(sQuery, aData)

    if 'recruitments' in aRequestData.keys():
        for aRecruitment in aRequestData['recruitments']:
            aData = ()

            if 'id' in aRecruitment.keys():
                sQuery=(
                    "UPDATE janksa.project_recruitments_prr"
                    " SET prr_description = %s"
                    " WHERE prr_fk_prj_id = %s"
                    " AND prr_id = %s"
                )

                aData = aData + (aRecruitment['description'],)
                aData = aData + (projectid,)
                aData = aData + (aRecruitment['id'],)

                Database.update(sQuery, aData)
            else:
                sQuery=(
                    "INSERT INTO janksa.project_recruitments_prr"
                    " (prr_fk_prj_id, prr_description)"
                    " VALUES (%s, %s)"
                )

                aData = aData + (projectid,)
                aData = aData + (aRecruitment['description'],)

                Database.insert(sQuery, aData)

    if 'news' in aRequestData.keys():
        aData = ()
        if 'id' in aRequestData['news'].keys():
            sQuery=(
                "UPDATE janksa.project_news_prn"
                " SET prn_content = %s"
                " WHERE prn_fk_prj_id = %s"
                " AND prn_id = %s"
            )

            aData = aData + (aRequestData['news']['content'],)
            aData = aData + (projectid,)
            aData = aData + (aRequestData['news']['id'],)

            Database.update(sQuery, aData)

        else:
            sQuery=(
                "INSERT INTO janksa.project_news_prn"
                " (prn_fk_prj_id, prn_content)"
                " VALUES (%s, %s)"
            )

            aData = aData + (projectid,)
            aData = aData + (aRequestData['news']['content'],)

            Database.insert(sQuery, aData)

    response = application.response_class(
        status=204,
        mimetype='application/json'
    )
    return response

### Discussions ################################################################

@application.route('/discussions/<token>', methods=['POST'])
def postDiscussions(token):
    """ Create a discussion """

    m = hashlib.sha1()
    key = "H5812*Ub48J!" + time.strftime("%d%m%Y")
    m.update(key.encode())
    if token != m.hexdigest():
        return "Not found", 404

    aRequestData = request.get_json()

    sQuery=(
        "INSERT INTO janksa.discussions_dsc"
        " (dsc_id)"
        " VALUES (NULL)"
    )

    aData = ()

    iDiscussionId = Database.insert(sQuery, aData)

    if iDiscussionId != None :

        if 'members' in aRequestData.keys():
            for iMember in aRequestData['members']:
                sQuery=(
                    "INSERT INTO janksa.x_mbr_dsc_xmbrdsc"
                    " (xmbrdsc_fk_dsc_id, xmbrdsc_fk_mbr_id)"
                    " VALUES (%s, %s)"
                )

                aData = ()
                aData = aData + (iDiscussionId,)
                aData = aData + (iMember,)

                Database.insert(sQuery, aData)

        if 'message' in aRequestData.keys():
            sQuery = (
                "INSERT INTO janksa.discussion_messages_dsm"
                " (dsm_fk_dsc_id, dsm_fk_mbr_id, dsm_message)"
                " VALUES (%s, %s, %s)"
            )

            aData = ()
            aData = aData + (iDiscussionId,)
            aData = aData + (aRequestData['message']['member'],)
            aData = aData + (aRequestData['message']['message'],)

            Database.insert(sQuery, aData)

        if 'recruitments' in aRequestData.keys():
            for aRecruitment in aRequestData['recruitments']:
                aData = ()

                sQuery=(
                    "INSERT INTO janksa.project_recruitments_prr"
                    " (prr_fk_prj_id, prr_description)"
                    " VALUES (%s, %s)"
                )

                aData = aData + (projectid,)
                aData = aData + (aRecruitment['description'],)

                Database.insert(sQuery, aData)

        response = application.response_class(
            status=201,
            mimetype='application/json'
        )
        response.headers['Content-Location'] = url_for('getDiscussions', discussionid = iDiscussionId)

    else:
        response = application.response_class(
            status=400,
            mimetype='application/json'
        )

    return response

@application.route('/discussions/<token>/<discussionid>', methods=['PUT'])
def putDiscussions(token, discussionid):
    """ Put a message on the discussion """

    m = hashlib.sha1()
    key = "H5812*Ub48J!" + time.strftime("%d%m%Y")
    m.update(key.encode())
    if token != m.hexdigest():
        return "Not found", 404

    aRequestData = request.get_json()

    sQuery = (
        "INSERT INTO janksa.discussion_messages_dsm"
        " (dsm_fk_dsc_id, dsm_fk_mbr_id, dsm_message)"
        " VALUES (%s, %s, %s)"
    )

    aData = ()
    aData = aData + (discussionid,)
    aData = aData + (aRequestData['member'],)
    aData = aData + (aRequestData['message'],)

    Database.insert(sQuery, aData)

    response = application.response_class(
        status=204,
        mimetype='application/json'
    )

    return response

@application.route('/discussions/<token>', methods=['GET'])
def getDiscussions(token):
    """ Get projects """

    m = hashlib.sha1()
    key = "H5812*Ub48J!" + time.strftime("%d%m%Y")
    m.update(key.encode())
    if token != m.hexdigest():
        return "Not found", 404

    aFilters = request.args.get('filters')

    aData = ()

    sQuery = (
        "SELECT dsc_id AS id"
        " FROM janksa.discussions_dsc"
    )

    if aFilters != None:
        aFilters = json.loads(aFilters)
        if 'member' in aFilters.keys():
            sQuery += " INNER JOIN janksa.x_mbr_dsc_xmbrdsc member1"
            sQuery += " ON member1.xmbrdsc_fk_dsc_id = dsc_id "
            sQuery += " AND member1.xmbrdsc_fk_mbr_id = %s"

            aData = aData + (aFilters['member'],)

            if 'secondMember' in aFilters.keys():
                sQuery += " INNER JOIN janksa.x_mbr_dsc_xmbrdsc member2"
                sQuery += " ON member2.xmbrdsc_fk_dsc_id = dsc_id "
                sQuery += " AND member2.xmbrdsc_fk_mbr_id = %s"
                sQuery += " AND member2.xmbrdsc_fk_mbr_id != member1.xmbrdsc_fk_mbr_id"

                aData = aData + (aFilters['secondMember'],)

    aDiscussions = Database.get(sQuery, aData)

    if len(aDiscussions) > 0:
        for aDiscussion in aDiscussions:
            sQuery = (
                "SELECT xmbrdsc_fk_mbr_id AS member"
                " FROM janksa.x_mbr_dsc_xmbrdsc"
                " WHERE xmbrdsc_fk_dsc_id = %s"
            )

            aData = ()
            aData = aData + (aDiscussion['id'],)

            aMembers = Database.get(sQuery, aData)

            aDiscussion['members'] = aMembers

        response = application.response_class(
            response=json.dumps(aDiscussions),
            status=200,
            mimetype='application/json'
        )

    else:
        response = application.response_class(
            response={},
            status=200,
            mimetype='application/json'
        )
    return response

@application.route('/discussions/<token>/<discussionid>', methods=['GET'])
def getDiscussion(token, discussionid):
    """ Get a project by his id """

    m = hashlib.sha1()
    key = "H5812*Ub48J!" + time.strftime("%d%m%Y")
    m.update(key.encode())
    if token != m.hexdigest():
        return "Not found", 404

    sQuery = (
        "SELECT dsc_id AS id"
        " FROM janksa.discussions_dsc"
        " WHERE dsc_id = %s"
    )
    aData = ()
    aData = aData + (discussionid,)

    aDiscussion = Database.get(sQuery, aData)

    if len(aDiscussion) > 0:

        sQuery = (
            "SELECT xmbrdsc_fk_mbr_id AS member"
            " FROM janksa.x_mbr_dsc_xmbrdsc"
            " WHERE xmbrdsc_fk_dsc_id = %s"
        )

        aData = ()
        aData = aData + (discussionid,)

        aMembers = Database.get(sQuery, aData)

        aDiscussion[0]['members'] = aMembers

        sQuery = (
            "SELECT dsm_id AS id"
            ", dsm_fk_mbr_id AS member"
            ", dsm_message AS message"
            ", dsm_created_at AS created_at"
            " FROM janksa.discussion_messages_dsm"
            " WHERE dsm_fk_dsc_id = %s"
            " ORDER BY dsm_created_at DESC"
        )

        aData = ()
        aData = aData + (discussionid,)

        aMessages = Database.get(sQuery, aData)

        aDiscussion[0]['messages'] = aMessages

        response = application.response_class(
            response=json.dumps(aDiscussion[0], default=datetime_handler),
            status=200,
            mimetype='application/json'
        )

    else:
        response = application.response_class(
            response={},
            status=200,
            mimetype='application/json'
        )
    return response

### Articles ###################################################################

@application.route('/articles/<token>', methods=['GET'])
def getArticles(token):
    """ Get all the articles """

    m = hashlib.sha1()
    key = "H5812*Ub48J!" + time.strftime("%d%m%Y")
    m.update(key.encode())
    if token != m.hexdigest():
        return "Not found", 404

    sQuery=(
        "SELECT art_id AS id"
        ", art_name AS name"
        ", art_picture AS picture"
        ", art_created_at AS date"
        " FROM janksa.articles_art"
    )

    aData = ()

    aRequestSort = request.args.get('sort_by')

    if aRequestSort != None:
        aRequestSort = json.loads(aRequestSort)
        sQuery += " ORDER BY"

        if aRequestSort['name'] == 'date':
            sQuery += " art_created_at"
        else:
            sQuery += " art_created_at"

        if aRequestSort['type'] == 'ASC':
            sQuery += " ASC"
        else:
            sQuery += " DESC"

    aRequestOffset = request.args.get('limit')

    if aRequestOffset != None:
        aRequestOffset = json.loads(aRequestOffset)

        sQuery += " LIMIT %s, %s"
        aData = aData + (aRequestOffset['min'],)
        aData = aData + (aRequestOffset['max'],)

    aRows = Database.get(sQuery, aData)

    response = application.response_class(
        response=json.dumps(aRows, default=datetime_handler),
        status=200,
        mimetype='application/json'
    )
    return response

@application.route('/articles/<token>', methods=['POST'])
def postArticles(token):
    """ Create a article """

    m = hashlib.sha1()
    key = "H5812*Ub48J!" + time.strftime("%d%m%Y")
    m.update(key.encode())
    if token != m.hexdigest():
        return "Not found", 404

    sQuery=(
        "INSERT INTO janksa.articles_art"
        " (art_name, art_picture, art_created_at)"
        " VALUES (%s, %s, NOW())"
    )

    aData = ()

    aRequestData = request.get_json()

    aData = aData + (aRequestData['name'],)
    aData = aData + (aRequestData['picture'],)

    iArticleid = Database.insert(sQuery, aData)

    if iArticleid != None :

        if 'description' in aRequestData.keys():
            aData = ()

            sQuery=(
                "INSERT INTO janksa.article_description_ard"
                " (ard_fk_art_id, ard_description)"
                " VALUES (%s, %s)"
            )

            aData = aData + (iArticleid,)
            aData = aData + (aRequestData['description'],)

            Database.insert(sQuery, aData)

        response = application.response_class(
            status=201,
            mimetype='application/json'
        )
        response.headers['Content-Location'] = url_for('getArticle', articleid=iArticleid)

    else:
        response = application.response_class(
            status=400,
            mimetype='application/json'
        )

    return response

@application.route('/articles/<token>/<articleid>', methods=['GET'])
def getArticle(token, articleid):
    """ Get a project by his id """

    m = hashlib.sha1()
    key = "H5812*Ub48J!" + time.strftime("%d%m%Y")
    m.update(key.encode())
    if token != m.hexdigest():
        return "Not found", 404

    sQuery = (
        "SELECT art_id AS id"
        ", art_name AS name"
        ", art_picture AS picture"
        ", IFNULL(ard_description,'') AS description"
        " FROM janksa.articles_art"
        " LEFT JOIN janksa.article_description_ard ON ard_fk_art_id = art_id"
        " WHERE art_id = %s"
    )

    aData = (articleid)

    aRow = Database.get(sQuery, aData)

    if len(aRow) > 0:
        response = application.response_class(
            response=json.dumps(aRow[0], default=datetime_handler),
            status=200,
            mimetype='application/json'
        )

    else:
        response = application.response_class(
            response={},
            status=200,
            mimetype='application/json'
        )
    return response

@application.route('/articles/<token>/<articleid>', methods=['PUT'])
def putArticle(token, articleid):
    """ Update a project by his id """

    m = hashlib.sha1()
    key = "H5812*Ub48J!" + time.strftime("%d%m%Y")
    m.update(key.encode())
    if token != m.hexdigest():
        return "Not found", 404

    aRequestData = request.get_json()

    if 'name' in aRequestData.keys():
        aData = ()

        sQuery = (
            "UPDATE janksa.articles_art"
            " SET art_name = %s"
            ", art_picture = %s"
            " WHERE art_id = %s"
        )

        aData = aData + (aRequestData['name'],)
        aData = aData + (aRequestData['picture'],)
        aData = aData + (articleid,)

        Database.update(sQuery, aData)

    if 'description' in aRequestData.keys():
        aData = ()

        sQuery=(
            "UPDATE janksa.article_description_ard"
            " SET ard_description = %s"
            " WHERE ard_fk_art_id = %s"
        )

        aData = aData + (aRequestData['description'],)
        aData = aData + (articleid,)

        Database.update(sQuery, aData)

    response = application.response_class(
        status=204,
        mimetype='application/json'
    )
    return response
